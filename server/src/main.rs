mod api;
mod errors;
mod app_data;
mod models;
mod serde_custom;

use actix_web::{HttpServer, web, Responder, HttpResponse, App, middleware::{Logger}};
use dotenv::dotenv;
use std::env;
use env_logger;
use crate::app_data::AppData;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    setup_logger();

    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .data(AppData::new())
            .route("/", web::get().to(index))
            .service(api::configure_api())
    })
        .bind(bind_address()).unwrap()
        .run()
        .await
}

fn bind_address() -> String {
    match env::var("BIND_ADDRESS") {
        Ok(var) => {
            var
        }
        Err(..) => {
            "127.0.0.1:8080".to_string()
        }
    }
}

fn setup_logger() {
    env_logger::init();
}


async fn index() -> impl Responder {
    HttpResponse::Ok()
        .body("<!DOCTYPE html>\
        <html lang=\"ru\">\
        <head>\
            <meta charset=\"UTF-8\">
            <title>Title</title>\
        </head>\
        <body>\
            <h1>Мой сайт<h1/>
        </body>\
        </html>")
}