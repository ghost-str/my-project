use deadpool_postgres::{Config, Pool};
use std::env;
use deadpool_postgres::tokio_postgres::NoTls;
use deadpool_postgres::config::ConfigError;

fn read_env_var(var: &str, default: &str) -> String {
    match env::var(var) {
        Ok(var) => var,
        Err(..) => default.to_string()
    }
}

fn configure_pool() -> Result<Pool, ConfigError> {
    let mut cfg = Config::new();
    cfg.dbname = Option::from(read_env_var("DATABASE_NAME", "postgres"));
    cfg.password = Option::from(read_env_var("DATABASE_PASSWORD", "root"));
    cfg.user = Option::from(read_env_var("DATABASE_USER", "root"));
    cfg.host = Option::from(read_env_var("DATABASE_HOST", "127.0.0.1"));
    let port: u16 = (read_env_var("DATABASE_PORT", "5432")).parse().unwrap();
    cfg.port = Option::from(port);
    cfg.create_pool(NoTls)
}

impl AppData {
    pub fn new() -> Self {
        AppData {
            db_pool: configure_pool().unwrap()
        }
    }
}

pub struct AppData {
    pub db_pool: Pool
}