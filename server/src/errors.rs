use std::fmt::{Debug};
use actix_web::{HttpResponse, ResponseError};
use derive_more::{Display};
use deadpool_postgres::PoolError;
use deadpool_postgres::tokio_postgres::Error;
use tokio_postgres::Error as TokioError;

#[derive(Display, Debug)]
pub enum AppErrors {
    NotFound,
    ParamError,
    InsertError(TokioError),
    SelectError(TokioError),
    PoolError(PoolError),
    ConnectionError(Error),
}

impl std::error::Error for AppErrors {}

impl ResponseError for AppErrors {
    fn error_response(&self) -> HttpResponse {
        match *self {
            AppErrors::NotFound => HttpResponse::NotFound().finish(),
            AppErrors::ParamError => HttpResponse::BadRequest().finish(),
            AppErrors::PoolError(ref err) => {
                HttpResponse::InternalServerError().body(err.to_string())
            }
            AppErrors::ConnectionError(ref err) => {
                HttpResponse::InternalServerError().body(err.to_string())
            }
            AppErrors::SelectError(ref err) => {
                HttpResponse::InternalServerError().body(err.to_string())
            }
            AppErrors::InsertError(ref err) => {
                HttpResponse::InternalServerError().body(err.to_string())
            }
        }
    }
}