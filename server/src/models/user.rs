use deadpool_postgres::{Client};
use crate::{errors::AppErrors};
use deadpool_postgres::tokio_postgres::Row;
use uuid::Uuid;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    #[serde(with = "crate::serde_custom::uuid")]
    id: Uuid,
    username: String,
    email: String,
    #[serde(skip_serializing)]
    password: String,
}

impl From<&Row> for User {
    fn from(row: &Row) -> Self {
        User {
            id: row.get("id"),
            username: row.get("username"),
            email: row.get("email"),
            password: row.get("password"),
        }
    }
}

pub struct Users {
    _rows: Vec<Row>,
    _index: usize,
}

impl Iterator for Users {
    type Item = User;

    fn next(&mut self) -> Option<Self::Item> {
        match self._rows.get(self._index) {
            None => None,
            Some(row) => {
                self._index += 1;
                return Some(User::from(row));
            }
        }
    }
}


pub async fn select_users(client: &Client) -> Result<Users, AppErrors> {
    let result: Vec<Row> = client
        .query("select * from users", &[])
        .await
        .map_err(AppErrors::ConnectionError)?;
    Ok(Users {
        _rows: result,
        _index: 0,
    })
}

pub async fn insert_user(client: &Client, user: &User) -> Result<Vec<Row>, AppErrors> {
    client
        .query(
            "insert into users values($1,$2,$3,$4) RETURNING *",
            &[
                &user.id,
                &user.username,
                &user.email,
                &user.password
            ],
        )
        .await
        .map_err(AppErrors::InsertError)
}

pub async fn get_by_id(client: &Client, id: &Uuid) -> Result<Option<User>, AppErrors> {
    let result = client
        .query(
            "select * from users where id=$1",
            &[
                &id
            ],
        )
        .await
        .map_err(AppErrors::SelectError)?;

    match result.get(0) {
        Some(row) => {
            let user = User::from(row);
            Ok(Some(user))
        }
        _ => {
            Ok(None)
        }
    }
}