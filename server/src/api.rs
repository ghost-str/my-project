use actix_web::{web, get, post, Scope, HttpResponse};
use deadpool_postgres::{Client};
use crate::{AppData, errors::AppErrors, models::user::{select_users, insert_user, get_by_id}};
use actix_web::web::Json;
use crate::models::user::User;
use uuid::{Uuid, Error};
use std::str::FromStr;

pub fn configure_api() -> Scope {
    web::scope("/api")
        .service(get_users)
        .service(create_user)
        .service(get_user)
}


#[get("/user")]
async fn get_users(app_data: web::Data<AppData>) -> Result<Json<Vec<User>>, AppErrors> {
    let client: Client = app_data.db_pool.get().await.map_err(AppErrors::PoolError)?;

    let users = select_users(&client).await?.collect();
    Ok(web::Json(users))
}

#[get("/user/{user_id}")]
async fn get_user(web::Path((user_id)): web::Path<(String)>, app_data: web::Data<AppData>) -> Result<Json<User>, AppErrors> {
    let client: Client = app_data.db_pool.get().await.map_err(AppErrors::PoolError)?;

    match Uuid::from_str(user_id.as_str()) {
        Err(..) => { Err(AppErrors::ParamError) }
        Ok(uuid_id) => {
            let user = get_by_id(&client, &uuid_id).await?;
            match user {
                Some(user) => {
                    Ok(web::Json(user))
                }
                _ => {
                    Err(AppErrors::NotFound)
                }
            }
        }
    }
}


#[post("/user")]
async fn create_user(user: web::Json<User>, app_data: web::Data<AppData>) -> Result<HttpResponse, AppErrors> {
    let client: Client = app_data.db_pool.get().await.map_err(AppErrors::PoolError)?;

    insert_user(&client, &user).await?;

    Ok(HttpResponse::Created().finish())
}